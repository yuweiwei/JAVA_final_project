
package ece155b.common;

import ece155b.common.Supply;
import java.io.StringReader;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

public class MessageParser extends DefaultHandler 
{
    public Message message;
    private Vector<Supply>supplyList = new Vector<Supply>();
    private Supply supply;
    private StringBuffer accumulator = new StringBuffer();
    
    /** Creates a new instance of MessageParser */
    public MessageParser(String xml, Message msg) {
        try {
            message = msg;
            SAXParserFactory factory = SAXParserFactory.newInstance(  );
            SAXParser parser = factory.newSAXParser();
            parser.parse(new InputSource(new StringReader(xml)), this);
        } catch (Exception ex) {
            System.out.println("Parsing error: "+ex);
        }
    }    
    
    public void characters(char[] buffer, int start, int length)
    {
        accumulator.append(buffer, start, length);
    }
    
    public void startElement(String uri,String lname, String name, Attributes attributes)
    {
    	if(name.equals ("Supply")){
			supply = new Supply();
		}	
		
        accumulator.setLength(0);
    }
    
    public void endElement(String uri,String lname,String name)
    {
        String value = accumulator.toString();
        if(name.equals("MessageType"))
            message.type = value;
        else if(name.equals("MessageFrom"))
            message.from = value;
        else if(name.equals("MessageTo"))
            message.to= value;
        else if(name.equals("MessageContent"))
            message.content = value;
        else if(name.equals("SupplyList"))
        	message.supplyList = supplyList;
        else if(name.equals("Supply"))
        	supplyList.add(supply);
        else if(name.equals("SupplyName"))
        	supply.name = value;
    	else if(name.equals("SupplyPrice"))
        	supply.price = Double.parseDouble(value);
    	else if(name.equals("SupplyAmountAvailable"))
        	supply.amountAvailable = Integer.parseInt(value);
            
    }
}
