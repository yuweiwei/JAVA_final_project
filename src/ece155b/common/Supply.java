package ece155b.common;

public class Supply {
	public String name;
	public double price;
	public int amountAvailable;
	
	public String toXML()
	{
		String xml="";
		xml += "<Supply>";
		xml += "<SupplyName>"+name+"</SupplyName>";
		xml += "<SupplyPrice>"+price+"</SupplyPrice>";
		xml += "<SupplyAmountAvailable>"+amountAvailable+"</SupplyAmountAvailable>";
		xml += "</Supply>";
		return xml;
	}
	public String toString()
	{
		String returnstr="";
		returnstr += "|| Supply\n";
			returnstr += "||| Name: "+name+"\n";
			returnstr += "||| Price: "+price+"\n";
			returnstr += "||| AmountAvailable: "+amountAvailable+"\n";
		return returnstr;
	}
	
}
