
package ece155b.common;

import java.util.Vector;

import ece155b.common.Supply;

public class Message {
	public String type;
	public String from = "";
	public String to = "";
	public String content;
	public Vector<Supply> supplyList;

	public Message(String xml) {
		new MessageParser(xml, this);
	}

	public Message() {

	}

	public String supplyListToString() {
		String returnstr = "";
		returnstr += "| SupplyList\n";
		for (int i = 0; i < supplyList.size(); i++)
			returnstr += ((Supply) supplyList.elementAt(i)).toString();
		return returnstr;
	}

	public String toXML() {
		String xml = "";
		xml = "<Message>";
		xml += "<MessageType>" + type + "</MessageType>";
		xml += "<MessageFrom>" + from + "</MessageFrom>";
		xml += "<MessageTo>" + to + "</MessageTo>";
		xml += "<MessageContent>" + content + "</MessageContent>";

		// if supply list request
		if (supplyList != null && supplyList.size() != 0) {
			xml += "<SupplyList>";
			for (int i = 0; i < supplyList.size(); i++)
				xml += ((Supply) supplyList.elementAt(i)).toXML();
			xml += "</SupplyList>";
		}

		xml += "</Message>";
		return xml;
	}

	public String toString() {
		String returnstr = "";
		returnstr += "Message:\nType:\t" + type + "\nFrom:\t" + from + "\nContent:\t" + content + "\n";
		if (supplyList != null && supplyList.size() != 0)
			returnstr += supplyListToString();

		return returnstr;
	}
}
