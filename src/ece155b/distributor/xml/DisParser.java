package ece155b.distributor.xml;

import ece155b.distributor.data.*;

import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class DisParser extends DefaultHandler
{
    private StringBuffer accumulator = new StringBuffer();
    
	Distributor distributor;
	SellSupply ssupply;
	NeedSupply nsupply;
	String tempName;
	Double price;
	int amount;

    public DisParser() 
    {
		distributor = new Distributor();
    }
    
    public Distributor getDistributor(){
    	return distributor;
    }
    
    public void characters(char[] buffer, int start, int length)
    {
        accumulator.append(buffer, start, length);
    }
    
    public void startElement(String uri,String lname, String name, Attributes attributes)
    {
		// - - - - - - - - - - - - - - - - - - - - -
		//	New SellSupply starts
		if(name.equals ("SellSupply"))
		{
			ssupply = new SellSupply();
		}	
		// - - - - - - - - - - - - - - - - - - - - -
		//	New NeedSupply starts
		else if(name.equals ("NeedSupply"))
		{
			nsupply = new NeedSupply();
		}
		// - - - - - - - - - - - - - - - - - - - - -
		// Reset accumulator
		accumulator.setLength(0);
    }
    
    public void endElement(String uri,String lname,String name)
    {
        String value = accumulator.toString();
        
        switch(name){
		// - - - - - - - - - - - - - - - - - - - - -
		// 	Company informations
		case "CompanyName":
			distributor.name = value;
			break;
		case "CompanyAddress":
			distributor.address = value;
			break;
		case "CompanyContact":
			distributor.contact = value;
			break;
		// - - - - - - - - - - - - - - - - - - - - -
		// 	Add sell supply and need supply objects
		case "SellSupply":
			ssupply.name = String.valueOf(tempName);
			ssupply.price = price;
			ssupply.amountAvailable = amount;
			distributor.addSellItem(ssupply);
			break;
		case "NeedSupply":
			nsupply.name = String.valueOf(tempName);			
			nsupply.amountNeeded = amount;
			distributor.addNeedItem(nsupply);
			break;
		// - - - - - - - - - - - - - - - - - - - - -
		// 	Set parameters of Supply Object
		case "SupplyName":
			tempName = value;
			break;
		case "SupplyPrice":
			price = Double.parseDouble(value);
			break;
		// - - - - - - - - - - - - - - - - - - - - -
		// 	Set parameters of SellSupply and NeedSupply Object
		case "SupplyAmountAvailable":
			amount = Integer.parseInt(value);
			break;			
		case "SupplyAmountNeeded":
			amount = Integer.parseInt(value);
			break;	
		}		
    }
    
}
