
package ece155b.distributor.data;

import java.util.Vector;

public class Distributor {
	public String name;
	public String address;
	public String contact; 
	
	Vector<SellSupply> sellitems;	// Vector of sell items
	Vector<NeedSupply> needitems;	// Vector of items needed

	public Distributor()
	{
		sellitems = new Vector ();
		needitems = new Vector ();		
	}

	public void addSellItem(SellSupply ss)
	{
		sellitems.add(ss);
	}

	public void addNeedItem(NeedSupply ns)
	{
		needitems.add(ns);
	}
	
	public Vector getSellItems()
	{
		return sellitems;
	}
	
	public Vector getNeedItems()
	{
		return needitems;
	}
	
	public void clearAllIteams(){
		sellitems.clear();
		needitems.clear();
	}

	public String toXML()
	{
		String xml="";
		xml += "<Distributor>";
		xml += "<CompanyInfo>";
		xml += "<CompanyName>"+name+"</CompanyName>";
		xml += "<CompanyAddress>"+address+"</CompanyAddress>";
		xml += "<CompanyContact>"+contact+"</CompanyContact>";
		xml += "</CompanyInfo>";

		xml += "<ItemsSold>";
		for (int i = 0; i<sellitems.size(); i++)
			xml += ((SellSupply)sellitems.elementAt(i)).toXML();
		xml += "</ItemsSold>";

		xml += "<ItemsNeeded>";
		for (int i = 0; i<needitems.size(); i++)
			xml += ((NeedSupply)needitems.elementAt(i)).toXML();
		xml += "</ItemsNeeded>";

		xml += "</Distributor>";
		return xml;
	}

	public String toString()
	{
		String returnstr="";
		returnstr += "| CompanyInfo\n";
		returnstr += "|| CompanyName: "+name+"\n";
		returnstr += "|| CompanyAddress: "+address+"\n";
		returnstr += "|| CompanyContact: "+contact+"\n";
		returnstr += "\n";

		returnstr += "| ItemsSold\n";
		for (int i = 0; i<sellitems.size(); i++)
			returnstr += ((SellSupply)sellitems.elementAt(i)).toString();
		returnstr += "\n";
		
		returnstr += "| ItemsNeeded\n";
		for (int i = 0; i<needitems.size(); i++)
			returnstr += ((NeedSupply)needitems.elementAt(i)).toString();
		returnstr += "\n";

		return returnstr;
	}
}
