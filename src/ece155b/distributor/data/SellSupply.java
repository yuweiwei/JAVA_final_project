package ece155b.distributor.data;

public class SellSupply
{
	public String name;
	public double price;
	public int amountAvailable;
	
	public String toXML()
	{
		String returnstr="";
		returnstr += "<SellSupply>";
			returnstr += "<SupplyName>"+name+"</SupplyName>";
			returnstr += "<SupplyPrice>"+price+"</SupplyPrice>";
			returnstr += "<SupplyAmountAvailable>"+amountAvailable+"</SupplyAmountAvailable>";
		returnstr += "</SellSupply>";
		return returnstr;
	}

	public String toString()
	{
		String returnstr="";
		returnstr += "|| SellSupply\n";
			returnstr += "||| SupplyName: "+name+"\n";
			returnstr += "||| SupplyPrice: $"+price+"\n";
			returnstr += "||| SupplyAmountAvailable: "+amountAvailable+"\n";
		return returnstr+"\n";
	}
}