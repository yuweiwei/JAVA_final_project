package ece155b.distributor.data;

public class NeedSupply
{
	public String name;	
	public int amountNeeded;
	
	public String toXML()
	{
		String returnstr="";
		returnstr += "<NeedSupply>";
			returnstr += "<SupplyName>"+name+"</SupplyName>";
			returnstr += "<SupplyAmountNeeded>"+amountNeeded+"</SupplyAmountNeeded>";
		returnstr += "</NeedSupply>";
		return returnstr;
	}

	public String toString()
	{
		String returnstr="";
		returnstr += "|| NeedSupply\n";
			returnstr += "||| SupplyName: "+name+"\n";
			returnstr += "||| SupplyAmountNeeded: "+amountNeeded+"\n";
		return returnstr+"\n";
	}
}