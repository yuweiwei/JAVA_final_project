package ece155b.distributor.data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import ece155b.distributor.xml.DisParser;

public class Distributor_Droid {
	public final String fileDir = System.getProperty("user.dir")+File.separator+"SavedXml"+File.separator+"distributors"+File.separator;
	public Distributor instance;
	
	public Distributor_Droid(){
		instance = new Distributor();
	}	
	
	public void toXmlFile(String fileName)
	{
		try
		{
			new File("SavedXml"+File.separator+"distributors").mkdirs();
			File outputFile = new File(fileDir + fileName + ".xml");
			BufferedWriter br = new BufferedWriter(new FileWriter(outputFile));
			br.write("<?xml version='1.0' ?>");
			br.write(instance.toXML());
			br.close();
		}
		catch (Exception ex)
		{
			System.out.println ("Exception:"+ex);
		}
	}

	public void readXmlFile(String url)
	{
		DisParser disParser = new DisParser();
		try
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			parser.parse(url, disParser);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		this.instance = disParser.getDistributor();
	}

	
}
