package ece155b.distributor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import ece155b.common.Common;
import ece155b.common.Message;
import ece155b.common.Supply;
import ece155b.distributor.comm.ConnHandler;
import ece155b.distributor.data.Distributor_Droid;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.Font;

public class PurchaseApp extends JFrame implements ActionListener {
	public DistributorApp dApp;
	private ConnHandler handler;
	private Container purchasePage;

	private JTable supply_table;
	private DefaultTableModel supplyModel;

	public Vector<Supply> supplyList;

	public PurchaseApp(DistributorApp dApp, ConnHandler handler, Vector<Supply> supplyList) {
		this.dApp = dApp;
		this.handler = handler;
		this.supplyList = supplyList;

		init();
	}

	private void init() {
		purchasePage = getContentPane();
		purchasePage.setBackground(new Color(255, 204, 204));
		purchasePage.setBounds(100, 100, 1200, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setAlwaysOnTop(true);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dApp.infoAppend("取消交易!");
				handler.removeAllProConn();
			}
		});

		setTitle("Purchase");
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		renderGUI();

		refreshTable();
	}

	private void renderGUI() {
		JLabel lblWhatDoYou = new JLabel("What do you want to buy?");
		lblWhatDoYou.setBounds(0, 15, 478, 39);
		lblWhatDoYou.setHorizontalAlignment(SwingConstants.CENTER);
		lblWhatDoYou.setFont(new Font("SetoFont", Font.PLAIN, 24));
		purchasePage.add(lblWhatDoYou);

		JButton buySupply_btn = new JButton("Buy");
		buySupply_btn.setBounds(166, 396, 111, 31);
		buySupply_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		buySupply_btn.addActionListener(this);
		purchasePage.add(buySupply_btn);

		supplyModel = new DefaultTableModel(new Object[][] { { "", "", "" } },
				new Object[] { "Name", "Price", "Available" });
		getContentPane().setLayout(null);
		supply_table = new JTable(supplyModel) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		supply_table.setBackground(Color.white);
		JScrollPane scrollPane = new JScrollPane(supply_table);
		scrollPane.setBounds(15, 63, 444, 318);
		scrollPane.setEnabled(false);
		purchasePage.add(scrollPane);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selectedRows = supply_table.getSelectedRows();
		Vector<Supply> buyList = new Vector<Supply>();
		for (int i = selectedRows.length - 1; i >= 0; i--) {
			Vector row = (Vector) supplyModel.getDataVector().elementAt(selectedRows[i]);
			Supply s = new Supply();
			s.name = row.get(0).toString();
			s.price = Double.parseDouble(row.get(1).toString());
			s.amountAvailable = Integer.parseInt(row.get(2).toString());
			buyList.add(s);
			supplyModel.removeRow(selectedRows[i]);
		}
		sendBuyRequest(buyList);
	}

	public void sendBuyRequest(Vector<Supply> buyList) {
		Message msg = new Message();
		msg.type = Common.REQUEST_PURCHASE;
		msg.from = dApp.getCompanyName();
		msg.supplyList = buyList;
		handler.broadcast(msg);

		dApp.infoAppend("The list you want to buy:\n" + msg.supplyListToString());
	}

	public void refreshTable() {
		for (int row = supplyModel.getRowCount() - 1; row >= 0; row--) {
			supplyModel.removeRow(row);
		}

		dApp.infoAppend(supplyList.toString());

		supplyList.forEach(s -> {
			supplyModel.addRow(new Object[] { s.name, s.price, s.amountAvailable });
		});
	}
}
