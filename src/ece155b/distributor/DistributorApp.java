
package ece155b.distributor;

import ece155b.common.*;
import ece155b.distributor.comm.ConnHandler;
import ece155b.distributor.data.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class DistributorApp extends JFrame implements ActionListener {
    public Distributor_Droid distributorDroid;
    private ConnHandler handler;    
    
	private Container mainPage;
	private JTextField companyName_textbox;
	private JTextField contactInfo_textbox;
	private JTextArea address_textbox;
	private JTable itemSold_table;
	private JTable itemNeeded_table;
	private DefaultTableModel itemSoldModel;
	private DefaultTableModel itemNeededModel;
	private JButton saveDistributor_btn;
	private JButton loadDistributor_btn;
	private JButton addSoldRow_btn;
	private JButton addNeededRow_btn;
	private JButton removeSoldRow_btn;
	private JButton removeNeededRow_btn;
	public JButton connentProvider_btn;
	private TextArea info_TextArea;
	private JLabel lblInfo;
    
    public static void main(String [] args) {
        new DistributorApp();
    }
    
    public DistributorApp() {
    	distributorDroid = new Distributor_Droid();  	
    	handler = new ConnHandler(this);      
    	
        init();
    }
    
	private void init() {
		mainPage = getContentPane();
		mainPage.setBackground(new Color(255, 204, 204));
		mainPage.setBounds(100, 100, 1200, 800);

		renderGUI();
		
        setTitle("Distributor");
        setSize(1068, 641);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        setVisible(true);
	}
    
	private void renderGUI() {
    	
    	JLabel lblNewLabel = new JLabel("Company Information");
    	lblNewLabel.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel.setBounds(40, 16, 238, 20);

		JLabel lblCompanyName = new JLabel("Company Name : ");
		lblCompanyName.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblCompanyName.setBounds(40, 52, 143, 20);

		JLabel lblNewLabel_1 = new JLabel("Contact Info : ");
		lblNewLabel_1.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(40, 88, 143, 20);

		JLabel lblAddress = new JLabel("Address :");
		lblAddress.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblAddress.setBounds(40, 124, 143, 20);
		mainPage.setLayout(null);
		getContentPane().setLayout(null);
		mainPage.add(lblNewLabel);
		mainPage.add(lblCompanyName);
		mainPage.add(lblNewLabel_1);
		mainPage.add(lblAddress);

		companyName_textbox = new JTextField();
		companyName_textbox.setBounds(189, 49, 261, 23);
		companyName_textbox.setColumns(10);
		mainPage.add(companyName_textbox);

		contactInfo_textbox = new JTextField();
		contactInfo_textbox.setColumns(10);
		contactInfo_textbox.setBounds(189, 86, 261, 23);
		mainPage.add(contactInfo_textbox);

		address_textbox = new JTextArea();
		address_textbox.setColumns(10);
		address_textbox.setBounds(189, 124, 261, 50);
		address_textbox.setBorder(BorderFactory.createLineBorder(Color.gray));
		address_textbox.setLineWrap(true);
		mainPage.add(address_textbox);

		JLabel lblNewLabel_2 = new JLabel("Items sold to customer");
		lblNewLabel_2.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(25, 248, 269, 29);
		mainPage.add(lblNewLabel_2);

		itemSoldModel = new DefaultTableModel(
				new Object[][]{
					{"name","0","0"}}, 
				new Object[]{"Name","Price","Available"});
		itemSold_table = new JTable(itemSoldModel);
		itemSold_table.setBackground(Color.white);
		JScrollPane scrollPane1 = new JScrollPane(itemSold_table);
		scrollPane1.setBounds(25, 276, 444, 203);
		mainPage.add(scrollPane1);		

		JLabel lblNewLabel_3 = new JLabel("Items needed from providers  ");
		lblNewLabel_3.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel_3.setBounds(582, 252, 339, 20);
		mainPage.add(lblNewLabel_3);

		removeSoldRow_btn = new JButton("Delete selected row");
		removeSoldRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		removeSoldRow_btn.setBounds(247, 479, 222, 29);
		removeSoldRow_btn.addActionListener(this);
		mainPage.add(removeSoldRow_btn);

		addNeededRow_btn = new JButton("Add a row");
		addNeededRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		addNeededRow_btn.setBounds(582, 479, 222, 29);
		addNeededRow_btn.addActionListener(this);
		mainPage.add(addNeededRow_btn);

		itemNeededModel = new DefaultTableModel(
				new Object[][]{
					{"name","0"}}, 
				new Object[]{"Name","Required"});
		itemNeeded_table = new JTable(itemNeededModel);
		itemNeeded_table.setBackground(Color.white);
		JScrollPane scrollPane2 = new JScrollPane(itemNeeded_table);
		scrollPane2.setBounds(582, 276, 444, 203);
		mainPage.add(scrollPane2);	

		removeNeededRow_btn = new JButton("Delete selected row");
		removeNeededRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		removeNeededRow_btn.setBounds(804, 479, 222, 29);
		removeNeededRow_btn.addActionListener(this);
		mainPage.add(removeNeededRow_btn);

		addSoldRow_btn = new JButton("Add a row"); 		
		addSoldRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		addSoldRow_btn.setBounds(25, 479, 222, 29);
		addSoldRow_btn.addActionListener(this);
		mainPage.add(addSoldRow_btn);

		saveDistributor_btn = new JButton("Save Distributor");
		saveDistributor_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		saveDistributor_btn.setBounds(148, 544, 200, 29);
		saveDistributor_btn.addActionListener(this);
		mainPage.add(saveDistributor_btn);

		loadDistributor_btn = new JButton("Load Distributor");
		loadDistributor_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		loadDistributor_btn.setBounds(434, 545, 200, 29);
		loadDistributor_btn.addActionListener(this);
		mainPage.add(loadDistributor_btn);
		
		info_TextArea = new TextArea();
		info_TextArea.setEditable(false);
		info_TextArea.setBounds(582, 43, 442, 203);
		mainPage.add(info_TextArea);
		
		lblInfo = new JLabel("Info:");
		lblInfo.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblInfo.setBounds(582, 19, 46, 15);
		mainPage.add(lblInfo);
		
		connentProvider_btn = new JButton("Connect Provider");
		connentProvider_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		connentProvider_btn.addActionListener(this);
		connentProvider_btn.setBounds(705, 545, 200, 29);
		mainPage.add(connentProvider_btn);

    }
    
	public void actionPerformed(ActionEvent e){		
		if(e.getSource() == addSoldRow_btn){
			addSoldRow_btn_Click();			
		}else if(e.getSource() == removeSoldRow_btn){
			removeSoldRow_btn_Click();	
		}else if(e.getSource() == addNeededRow_btn){
			addNeededRow_btn_Click();	
		}else if(e.getSource() == removeNeededRow_btn){
			removeNeededRow_btn_Click();	
		}else if(e.getSource() == saveDistributor_btn){
			saveProvider_btn_Click();	
		}else if(e.getSource() == loadDistributor_btn){
			loadProvider_btn_Click();	
		}else if(e.getSource() == connentProvider_btn){
			connentProvider_btn_Click();	
		}
	}
	
	private void addSoldRow_btn_Click(){
		itemSoldModel.addRow(new Object[]{"","",""});
	}

	private void removeSoldRow_btn_Click(){
		int[] selectedRows = itemSold_table.getSelectedRows();
		for(int i=selectedRows.length-1;i>=0;i--){
			itemSoldModel.removeRow(selectedRows[i]);	
		}		 
	}

	private void addNeededRow_btn_Click(){
		itemNeededModel.addRow(new Object[]{"","",""});
	}

	private void removeNeededRow_btn_Click(){
		int[] selectedRows = itemNeeded_table.getSelectedRows();
		for(int i=selectedRows.length-1;i>=0;i--){
			itemNeededModel.removeRow(selectedRows[i]);	
		}
	}
	
	private void saveProvider_btn_Click(){
		try{
			// - - - - - - - - - - - - - - - - - - - - -
			//save distributor value
			String tName = companyName_textbox.getText();
			distributorDroid.instance.name = tName;
			distributorDroid.instance.contact = contactInfo_textbox.getText();
			distributorDroid.instance.address = address_textbox.getText();
			distributorDroid.instance.clearAllIteams();

			for(Object o:itemSoldModel.getDataVector()){
				Vector v = (Vector) o;			
				SellSupply ss = new SellSupply();
				ss.name = v.get(0).toString();
				ss.price = Double.parseDouble(v.get(1).toString());
				ss.amountAvailable = Integer.parseInt(v.get(2).toString());
				distributorDroid.instance.addSellItem(ss);			
			}

			for(Object o:itemNeededModel.getDataVector()){
				Vector v = (Vector) o;
				NeedSupply ns = new NeedSupply();
				ns.name = v.get(0).toString();
				ns.amountNeeded = Integer.parseInt(v.get(1).toString());
				distributorDroid.instance.addNeedItem(ns);			
			}
			infoAppend("將保存以下資訊*********\n" + distributorDroid.instance.toString());

			// - - - - - - - - - - - - - - - - - - - - -
			//save process
			String toSaveFileName = JOptionPane.showInputDialog(mainPage, "幫檔案取個名字吧\n(預設為: \"" + tName +"\")");
			if(toSaveFileName == null){
				return;
			}else if(toSaveFileName.isEmpty()){
				toSaveFileName = tName;
			}

			if(new File(distributorDroid.fileDir + toSaveFileName).exists())
			{
				String[] options={"蓋掉它","還是算了"};				
				int opt = JOptionPane.showOptionDialog(mainPage, 
						"有一樣的檔案了，要不要覆蓋，要嗎要嗎?", 
						"訊息", 
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						"蓋掉它");
				if(opt == JOptionPane.NO_OPTION){
					return;
				}
			}
			infoAppend("檔案已保存，檔名為: \""+toSaveFileName+"\"");
			distributorDroid.toXmlFile(toSaveFileName);
			
		} catch(NumberFormatException e){
			JOptionPane.showMessageDialog(mainPage, "你看看是不是表格有空白 (◔ д◔)\n不然就是該打數字 你打成文字");
		} catch(Exception e){
			JOptionPane.showMessageDialog(mainPage, e);		
		}
	}
	
	private void loadProvider_btn_Click(){
		try{
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(distributorDroid.fileDir));
			int returnValue = fileChooser.showOpenDialog(mainPage);
			if (returnValue == JFileChooser.APPROVE_OPTION) 
			{ 
				File selectedFile = fileChooser.getSelectedFile();
				distributorDroid.readXmlFile(selectedFile.getAbsolutePath());
				infoAppend("載入檔案，檔名為: \""+selectedFile.getName()+"\"");
			} 
			// - - - - - - - - - - - - - - - - - - - - -
			//set company's info
			companyName_textbox.setText(distributorDroid.instance.name);
			contactInfo_textbox.setText(distributorDroid.instance.contact);
			address_textbox.setText(distributorDroid.instance.address);
			// - - - - - - - - - - - - - - - - - - - - -
			//clean and add supply to table
			Vector<SellSupply> sellitems = distributorDroid.instance.getSellItems();

			for(int row=itemSoldModel.getRowCount()-1;row>=0;row--){
				itemSoldModel.removeRow(row);
			}

			sellitems.forEach(si -> {			
				itemSoldModel.addRow(new Object[]{
						si.name, 
						si.price, 
						si.amountAvailable
				});
			});
			// - - - - - - - - - - - - - - - - - - - - -
			//clean and add NeedSupply to table
			Vector<NeedSupply> needitems = distributorDroid.instance.getNeedItems();


			for(int row=itemNeededModel.getRowCount()-1;row>=0;row--){
				itemNeededModel.removeRow(row);
			}

			needitems.forEach(ni -> {			
				itemNeededModel.addRow(new Object[]{
						ni.name, 
						ni.amountNeeded
				});
			});	
			
		} catch(Exception e){
			JOptionPane.showMessageDialog(mainPage, e);
		}
	}
	
	private void connentProvider_btn_Click(){
        try{
        	connentProvider_btn.setEnabled(false);
               
    		String providerPort = JOptionPane.showInputDialog(mainPage, "請輸入您想訪問的供應商的port號 (◔ д◔)\n(預設為\"1111\")");
    		
    		String companyName = getCompanyName();
    		if(providerPort != null &&  !companyName.isEmpty()){
    			if(providerPort.isEmpty()) providerPort = "1111";
            	ProviderContact contact = new ProviderContact();
            	contact.URL = "localhost";
            	contact.PORT = Integer.parseInt(providerPort);
    			handler.connectToProvider(contact);
    			
                Message msg = new Message();
                msg.type = Common.AUTHENTICATE_DISTRIBUTOR;
                msg.from = companyName;      
                msg.to = contact.Name;
                msg.content = "I want to buy something!(*´∀`)~♥";
                handler.broadcast(msg);
                
                infoAppend("Request Port: \""+providerPort+"\"");            
    		}else if(providerPort !=null && companyName.isEmpty()){
    			connentProvider_btn.setEnabled(true);
    			JOptionPane.showMessageDialog(mainPage, "買東西之前要先取名字啊!!! (#`Д´)ﾉ");	
    		} else connentProvider_btn.setEnabled(true);
        }catch(NumberFormatException nfe){
        	connentProvider_btn.setEnabled(true);
			JOptionPane.showMessageDialog(mainPage, "只能打數字啦!!! (#`Д´)ﾉ");		
		}catch(Exception e){
        	connentProvider_btn.setEnabled(true);
			JOptionPane.showMessageDialog(mainPage, e);		
		}
	}
	
	public String getCompanyName(){
		return companyName_textbox.getText();
	}
	
    public void infoAppend(String str) {
    	info_TextArea.append(str + "\n");
    }
}
