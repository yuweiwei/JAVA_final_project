package ece155b.distributor.comm;

import ece155b.common.Message;
import ece155b.provider.data.*;
import ece155b.distributor.data.*;
import java.io.*;
import java.net.*;

class ConnListener extends Thread{
    public BufferedWriter bwrite;
    public BufferedReader bread;
    public Socket socket;
    private ConnHandler PARENT;
    private ProviderContact contact; 
    
    
    ConnListener(ConnHandler p, Socket socket, ProviderContact pro) {
        PARENT 	= p;
        contact = pro;
        this.socket = socket;
        try 
        {            
            bwrite = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
            bread = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            start();
        } catch (Exception ex) {
        	PARENT.dApp.connentProvider_btn.setEnabled(true);
            System.out.println("Error with socket creation!");
        }
    }
    
    public void run() {
        try {
            while(true) {
                System.out.println("waiting");
                String xml 	= (String) bread.readLine();
                if(xml == null) break;
                PARENT.processMessage(xml, this);
            }
        } catch(Exception e) {
            // This may happen because of the fact that client
            // application is closed.
            System.out.println("Error reading message!");
        }
        PARENT.removeProConn(this);
        PARENT.dApp.connentProvider_btn.setEnabled(true);
        System.out.println("Socket closed!");
    }
    
    public void sendMessage(Message msg) {
        try {
            System.out.println("Sending to "+contact.Name+" \n"+msg);
            bwrite.write(msg.toXML());
            bwrite.newLine();
            bwrite.flush();
        } catch (Exception ex) {
            System.out.println("Error sending message");
        }
    }
    
    
}