package ece155b.distributor.comm;

import ece155b.common.Common;
import ece155b.common.Message;
import ece155b.distributor.*;
import ece155b.distributor.data.*;

import java.io.IOException;
import java.net.*;
import java.util.Vector;

import javax.swing.JOptionPane;

public class ConnHandler {
	Vector<ConnListener> providers; // vector that keeps track of connected
									// clients
	DistributorApp dApp;

	public ConnHandler(DistributorApp Appl) {
		providers = new Vector<ConnListener>();
		dApp = Appl;
	}

	public void connectToProvider(ProviderContact pro) {
		try {
			Socket socket = new Socket(pro.URL, pro.PORT);
			providers.addElement(new ConnListener(this, socket, pro));
		} catch (Exception ex) {
			dApp.connentProvider_btn.setEnabled(true);
			JOptionPane.showMessageDialog(dApp, "連線失敗!");	
			ex.printStackTrace();
		}
	}

	public void removeProConn(ConnListener cl) {
		try {
			cl.socket.close();
			providers.remove(cl);
		} catch (IOException e) {
			System.out.println("error occured in removeProConn : " + e.getMessage());
		}
	}

	public void removeAllProConn() {
		try {
			for (int i = 0; i < providers.size(); i++) {
				removeProConn((ConnListener) providers.elementAt(i));
			}
		} catch (Exception e) {
			dApp.infoAppend("交易失敗!");
		}
	}

	public void broadcast(Message m) {
		System.out.println("BCAST #" + providers.size());

		if (providers.size() == 0)
			System.out.println("No peer to broadcast");
		else
			for (int i = 0; i < providers.size(); i++) {
				ConnListener cl = (ConnListener) providers.elementAt(i);
				cl.sendMessage(m);
			}
	}

	protected synchronized void processMessage(String xml, ConnListener listener) {
		Message msg = new Message(xml);

		if (msg.type.equals(Common.BROADCAST)) {
			dApp.infoAppend(msg.toString());
		} else if (msg.type.equals(Common.AUTHENTICATE_DISTRIBUTOR_ACCEPT)) {
			authenticate_accept(msg);
		} else if (msg.type.equals(Common.AUTHENTICATE_DISTRIBUTOR_DENY)) {
			authenticate_deny(msg);
		} else if (msg.type.equals(Common.REQUEST_SUPPLY_LIST_REPLY)) {
			requsetSupplyList_reply(msg);
		} else if (msg.type.equals(Common.REQUEST_PURCHASE_REPLY)) {
			requestPurchase_reply(msg);
		} else
			System.out.println("Unknown message type");
		System.out.println(msg.toString());
	}


	private void authenticate_accept(Message msg) {
		dApp.infoAppend("Reply: " + msg.content);

		Message toRequestMsg = new Message();
		toRequestMsg.type = Common.REQUEST_SUPPLY_LIST;
		toRequestMsg.from = dApp.getCompanyName();
		toRequestMsg.content = "I want to get supply list!(*´∀`)~♥";
		broadcast(toRequestMsg);
	}

	private void authenticate_deny(Message msg) {
		JOptionPane.showMessageDialog(dApp, "您被拒絕了(っ・Д・)っ\n對方說: " + msg.content);
		dApp.infoAppend("Reply: " + msg.content);
		dApp.connentProvider_btn.setEnabled(true);
	}

	private void requsetSupplyList_reply(Message msg) {
		dApp.infoAppend("成功取得supply list");
		new PurchaseApp(dApp, this, msg.supplyList);
	}

	private void requestPurchase_reply(Message msg) {
		dApp.infoAppend(msg.content);
	}
}