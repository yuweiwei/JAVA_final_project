package ece155b.provider.xml;

import ece155b.common.Supply;
import ece155b.distributor.data.SellSupply;
import ece155b.provider.data.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class ProParser extends DefaultHandler
{
    private StringBuffer accumulator = new StringBuffer();
    
    Provider provider;
    Supply supply;
        
    public ProParser() 
    {
    	provider = new Provider();    	
    }
    
    public Provider getProvider(){
    	return this.provider;
    }
    
    public void characters(char[] buffer, int start, int length)
    {
        accumulator.append(buffer, start, length);
    }
    
    public void startElement(String uri,String lname, String name, Attributes attributes)
    {
		// - - - - - - - - - - - - - - - - - - - - -
		//	New Supply starts
		if(name.equals ("Supply"))
		{
			supply = new Supply();
		}	
		accumulator.setLength(0);
    }
    
    public void endElement(String uri,String lname,String name)
    {
		String value = accumulator.toString();

		switch(name)
		{
			// - - - - - - - - - - - - - - - - - - - - -
			// 	Company informations
			case "CompanyName":
				provider.name = value;
				break;
			case "CompanyAddress":
				provider.address = value;
				break;
			case "CompanyContact":
				provider.contact = value;
				break;
			// - - - - - - - - - - - - - - - - - - - - -
			// 	Add supply to supply list
			case "Supply":
				provider.addSupply(supply);
				break;

			// - - - - - - - - - - - - - - - - - - - - -
			// 	Set parameters of Supply Object
			case "SupplyName":
				supply.name = value;
				break;
			case "SupplyPrice":
				supply.price = Double.parseDouble(value);
				break;
			case "SupplyAmountAvailable":
				supply.amountAvailable = Integer.parseInt(value);
				break;
		}	        
    }    
}
