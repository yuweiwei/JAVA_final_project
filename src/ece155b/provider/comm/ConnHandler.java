package ece155b.provider.comm;

import ece155b.common.Common;
import ece155b.common.Message;
import ece155b.common.Supply;
import ece155b.provider.ProviderApp;
import java.io.IOException;
import java.net.*;
import java.util.Vector;

import javax.swing.JOptionPane;

public class ConnHandler extends Thread {
	private ServerSocket servers;
	Vector<ConnListener> handles; // vector that keeps track of connected
									// clients

	ProviderApp server; // reference to main application
						// ** you need to access doctor information

	public ConnHandler(ProviderApp proApp, int PortNo) {
		boolean isServerOK = false;
		while (!isServerOK) {
			try {
				server = proApp;
				servers = new ServerSocket(PortNo);
				handles = new Vector<ConnListener>();

				start();
				isServerOK = true;
			} catch (BindException be) {
				if (PortNo < 3000) {
					PortNo++;
					proApp.setPortNum(PortNo);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void run() {
		try {
			while (true) {
				System.out.println("Waiting connections...");
				Socket socket = servers.accept();
				System.out.println("Got one connection :) ");
				addUser(socket);
				System.out.println("Processed..");
			}
		} catch (Exception ex) {
			System.out.println("stop waiting!");
			System.out.println(ex.getMessage());
		}
	}

	// add new client user
	/**
	 * Here you might assign unique session id to client listener
	 */
	public void addUser(Socket socket) {
		handles.addElement(new ConnListener(this, socket));
	}

	/*
	 * You need a find criteria. Might be patient name, something unique to that
	 * patient (like ssn). Or you might assign session_ids to each connecting
	 * client and keep this in ClientListener object. Think that session id as
	 * your search criteria
	 **/
	public ConnListener findUser(String distributorName) {
		return null;
	}

	// Find user and remove..
	/**
	 * When client disconnects, or closes the connection the client listener
	 * will catch an exception. You can use that and inform the connHandler to
	 * remove respective client listener
	 */
	public void removeUser(ConnListener client) {
		try {
			client.socket.close();
			handles.remove(client);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send message to all clients, you might also write function to send
	 * specific client just go over the vector, find matching ClientListener
	 * object
	 *
	 * You might need to add another data field to ClientListener so that you
	 * will distingush between different clients.
	 */
	public void broadcast(Message m) {
		System.out.println("BCAST #" + handles.size());

		if (handles.size() == 0)
			System.out.println("No peer to broadcast");
		else
			for (int i = 0; i < handles.size(); i++) {
				ConnListener cl = (ConnListener) handles.elementAt(i);
				cl.sendMessage(m);
			}
	}

	/**
	 * YOu will need to modify this method, include all the cases you might have
	 * as message type.
	 *
	 * This method is synchronized for the following reasons: 1. Two clients
	 * might send message at the same time, we want to process one of the client
	 * and then process second. Otherwise, this might cause data incosistencies
	 *
	 * When you make a method synchronized, only one execution of the method
	 * exists, since all client handlers calls this method to process received
	 * messages, we are sure, two message calls will not be executed at the same
	 * time. One of them will be picked by the OS, and other will be pending
	 * until executing thread finishes its job.
	 *
	 * This is important to understand, otherwise, you might end up with
	 * inconsistent data.
	 *
	 * Wow.. so long :) take it serious. /
	 **/
	public synchronized void processMessage(String xml, ConnListener listener) {

		Message msg = new Message(xml);

		/*
		 * You will need to define message types, you might consider to have a
		 * Common.java class, which you keep all the constants that will be used
		 * by other java classes.
		 * 
		 * Most time I do that way, and it is really helpful.
		 * 
		 * For example: Message type will be used by both Doctor and Patient.
		 * Why not keep them in Common.java file.
		 * 
		 * First parse the received message, which is in XML format. Having
		 * message properties, like type, etc.
		 */
		if (msg.type.equals(Common.BROADCAST)) {
			// server.append(msg.toString());
		} else if (msg.type.equals(Common.AUTHENTICATE_DISTRIBUTOR)) {
			// only one transaction is allowed
			if (handles.size() > 1) {
				Message toReplyMsg = new Message();
				toReplyMsg.type = Common.AUTHENTICATE_DISTRIBUTOR_DENY;
				toReplyMsg.from = server.getProviderName();
				toReplyMsg.content = "There is already an transaction! ( ˘•ω•˘ )";
				listener.sendMessage(toReplyMsg);
				removeUser(listener);
				return;
			}
			authenticate_event(msg, listener);
		} else if (msg.type.equals(Common.REQUEST_SUPPLY_LIST)) {
			requestSupplyList_event(msg, listener);
		} else if(msg.type.equals(Common.REQUEST_PURCHASE)){
			requestPurchase_event(msg, listener);
		}else
			System.out.println("Unknown message type");
	}


	private void requestSupplyList_event(Message msg, ConnListener listener) {
		server.refreshSupplyList();
		Message toReplyMsg = new Message();
		toReplyMsg.type = Common.REQUEST_SUPPLY_LIST_REPLY;
		toReplyMsg.from = server.getProviderName();
		toReplyMsg.supplyList = server.providerDroid.instance.getSupplyList();
		listener.sendMessage(toReplyMsg);
		System.out.print(toReplyMsg.toXML());
	}

	private void authenticate_event(Message msg, ConnListener listener) {

		String[] options = { "接受他", "請他滾" };

		int opt = JOptionPane.showOptionDialog(server, "\"" + msg.from + "\" 想要來買東西，您要讓他買嗎?", "訊息",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, "接受他");

		if (opt == JOptionPane.YES_OPTION) {
			server.infoAppend("\"" + msg.from + "\" 來買東西囉~(✪ω✪)");
			listener.distributorName = msg.from;

			Message toReplyMsg = new Message();
			toReplyMsg.type = Common.AUTHENTICATE_DISTRIBUTOR_ACCEPT;
			toReplyMsg.from = server.getProviderName();
			toReplyMsg.content = "OK! You can buy whatever you want!(*´∀`)~♥";
			listener.sendMessage(toReplyMsg);

		} else if (opt == JOptionPane.NO_OPTION) {
			server.infoAppend("您把 \"" + listener.distributorName + "\" 給趕走了! (ﾟдﾟ≡ﾟдﾟ)");
			Message toReplyMsg = new Message();
			toReplyMsg.type = Common.AUTHENTICATE_DISTRIBUTOR_DENY;
			toReplyMsg.from = server.getProviderName();
			toReplyMsg.content = "本公司恕不招待!(╯°Д°)╯ ┻━┻";
			listener.sendMessage(toReplyMsg);
			removeUser(listener);
		}
	}
	
	private void requestPurchase_event(Message msg, ConnListener listener) {
		server.addToSoldList(msg.from, msg.supplyList);		
		
		Message toReplyMsg = new Message();
		toReplyMsg.type = Common.REQUEST_PURCHASE_REPLY;
		toReplyMsg.from = server.getProviderName();      
		toReplyMsg.content = "Purchased Successfully!(*´∀`)~♥";
        broadcast(toReplyMsg);
	}

}