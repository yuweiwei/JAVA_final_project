package ece155b.provider.data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import ece155b.provider.xml.ProParser;
import ece155b.common.Supply;

public class Provider_Droid {
	public final String fileDir = System.getProperty("user.dir") + File.separator + "SavedXml" + File.separator
			+ "providers" + File.separator;
	public Provider instance;

	public Provider_Droid() {
		instance = new Provider();
	}

	public void toXmlFile(String fileName) {
		try {
			new File("SavedXml" + File.separator + "providers").mkdirs();
			File outputFile = new File(fileDir + fileName + ".xml");
			BufferedWriter br = new BufferedWriter(new FileWriter(outputFile));
			br.write("<?xml version='1.0' ?>");
			br.write(instance.toXML());
			br.close();
		} catch (Exception ex) {
			System.out.println("Exception:" + ex);
		}
	}

	public void readXmlFile(String url) {
		ProParser proParser = new ProParser();
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			parser.parse(url, proParser);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		this.instance = proParser.getProvider();
	}

	private void test() {
		instance.name = "The Company";
		instance.address = "Address";
		instance.contact = "Contact me";
		Supply s = new Supply();
		s.name = "apple";
		s.price = 100;
		s.amountAvailable = 5;
		instance.addSupply(s);
	}

}
