package ece155b.provider.data;

import ece155b.common.Supply;
import java.util.Vector;

public class Provider {
    
	public String name;
	public String address;
	public String contact;    
    
    Vector<Supply> supplyList;
    
    public Provider()
    {
        supplyList = new Vector<Supply>();
    }
    
    public void addSupply(Supply supply)
    {
        supplyList.add(supply);
    }
    
    public void removeSupply(Supply supply){
    	supplyList.remove(supply);
    }
    
	public Vector getSupplyList()
	{
		return supplyList;
	}
	
	public void clearSupplyList(){		
		supplyList.clear();
	}
	
    public String toXML()
    {
        String xml = "<Provider>";
		xml += "<CompanyInfo>";
		xml += "<CompanyName>"+name+"</CompanyName>";
		xml += "<CompanyAddress>"+address+"</CompanyAddress>";
		xml += "<CompanyContact>"+contact+"</CompanyContact>";
		xml += "</CompanyInfo>";

		xml += "<SupplyList>";
		for (int i = 0; i<supplyList.size(); i++)
			xml += ((Supply)supplyList.elementAt(i)).toXML();
		xml += "</SupplyList>";

        xml += "</Provider>";
        
        return xml;
    }  
    
	public String toString()
	{
		String returnstr="";
		returnstr += "| CompanyInfo\n";
		returnstr += "|| CompanyName: "+name+"\n";
		returnstr += "|| CompanyAddress: "+address+"\n";
		returnstr += "|| CompanyContact: "+contact+"\n";
		returnstr += "\n";

		returnstr += "| SupplyList\n";
		for (int i = 0; i<supplyList.size(); i++)
			returnstr += ((Supply)supplyList.elementAt(i)).toString();
		returnstr += "\n";

		return returnstr;
	}
	
}