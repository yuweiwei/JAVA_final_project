package ece155b.provider;

import ece155b.common.Supply;
import ece155b.provider.comm.*;
import ece155b.provider.data.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ProviderApp extends JFrame implements ActionListener {

	public Provider_Droid providerDroid;
	private int PortNo;

	private ConnHandler connh;

	private Container mainPage;
	private JTextField companyName_textbox;
	private JTextField contactInfo_textbox;
	private JTextArea address_textbox;
	private JTable itemSold_table;
	private JTable supply_table;
	private DefaultTableModel soldModel;
	private DefaultTableModel supplyModel;
	private JButton saveProvider_btn;
	private JButton loadProvider_btn;
	private JButton addSupplyRow_btn;
	private JButton removeSupplyRow_btn;
	private TextArea info_TextArea;
	private JButton removeSoldSupplyRow_btn;
	private JButton addSoldSupplyRow_btn;
	private JLabel portNumber_label;

	public static void main(String[] args) {
		new ProviderApp();
	}

	public ProviderApp() {

		providerDroid = new Provider_Droid();

		init();

		PortNo = 1111;
		connh = new ConnHandler(this, PortNo);
	}

	public void init() {
		mainPage = getContentPane();
		mainPage.setBackground(new Color(204, 255, 255));
		mainPage.setBounds(100, 100, 1200, 800);

		renderGUI();

		setTitle("Provider");
		setSize(1068, 641);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setVisible(true);
	}

	public void renderGUI() {

		JLabel lblNewLabel = new JLabel("Company Information");
		lblNewLabel.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel.setBounds(40, 16, 209, 20);

		JLabel lblCompanyName = new JLabel("Company Name : ");
		lblCompanyName.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblCompanyName.setBounds(40, 52, 149, 20);

		JLabel lblNewLabel_1 = new JLabel("Contact Info : ");
		lblNewLabel_1.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(40, 88, 149, 20);

		JLabel lblAddress = new JLabel("Address :");
		lblAddress.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblAddress.setBounds(40, 134, 149, 20);
		getContentPane().setLayout(null);
		getContentPane().setLayout(null);
		mainPage.add(lblNewLabel);
		mainPage.add(lblCompanyName);
		mainPage.add(lblNewLabel_1);
		mainPage.add(lblAddress);

		companyName_textbox = new JTextField();
		companyName_textbox.setBounds(189, 49, 261, 23);
		companyName_textbox.setColumns(10);
		mainPage.add(companyName_textbox);

		contactInfo_textbox = new JTextField();
		contactInfo_textbox.setBounds(189, 86, 261, 23);
		contactInfo_textbox.setColumns(10);
		mainPage.add(contactInfo_textbox);

		address_textbox = new JTextArea();
		address_textbox.setBounds(189, 135, 261, 50);
		address_textbox.setColumns(10);
		address_textbox.setBorder(BorderFactory.createLineBorder(Color.gray));
		address_textbox.setLineWrap(true);
		mainPage.add(address_textbox);

		JLabel lblNewLabel_2 = new JLabel("Sold Supplys");
		lblNewLabel_2.setFont(new Font("SetoFont", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(27, 267, 269, 29);
		mainPage.add(lblNewLabel_2);
		soldModel = new DefaultTableModel(new Object[][] {  },
				new Object[] { "Company name", "Supply name", "Amount" });
		itemSold_table = new JTable(soldModel);
		itemSold_table.setBackground(Color.white);
		JScrollPane scrollPane2 = new JScrollPane(itemSold_table);
		scrollPane2.setBounds(27, 292, 444, 203);
		mainPage.add(scrollPane2);

		JLabel label_1 = new JLabel("Supplys for selling");
		label_1.setFont(new Font("SetoFont", Font.PLAIN, 18));
		label_1.setBounds(510, 267, 269, 29);
		getContentPane().add(label_1);
		supplyModel = new DefaultTableModel(new Object[][] { { "name", "0", "0" } },
				new Object[] { "Name", "Price", "Available" });
		removeSupplyRow_btn = new JButton("Delete selected row");
		removeSupplyRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		removeSupplyRow_btn.setBounds(732, 495, 222, 29);
		removeSupplyRow_btn.addActionListener(this);

		saveProvider_btn = new JButton("Save Provider");
		saveProvider_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		saveProvider_btn.setBounds(297, 548, 174, 29);
		saveProvider_btn.addActionListener(this);
		supply_table = new JTable(supplyModel);
		supply_table.setBackground(Color.white);
		JScrollPane scrollPane1 = new JScrollPane(supply_table);
		scrollPane1.setBounds(510, 292, 444, 203);
		mainPage.add(scrollPane1);
		mainPage.add(saveProvider_btn);
		mainPage.add(removeSupplyRow_btn);

		addSupplyRow_btn = new JButton("Add a row");
		addSupplyRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		addSupplyRow_btn.setBounds(510, 495, 222, 29);
		addSupplyRow_btn.addActionListener(this);
		mainPage.add(addSupplyRow_btn);

		loadProvider_btn = new JButton("Load Provider");
		loadProvider_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		loadProvider_btn.setBounds(510, 548, 174, 29);
		loadProvider_btn.addActionListener(this);
		mainPage.add(loadProvider_btn);

		info_TextArea = new TextArea();
		info_TextArea.setEditable(false);
		info_TextArea.setBounds(510, 48, 442, 203);
		mainPage.add(info_TextArea);

		JLabel label = new JLabel("Info:");
		label.setFont(new Font("SetoFont", Font.PLAIN, 18));
		label.setBounds(510, 12, 269, 29);
		getContentPane().add(label);

		addSoldSupplyRow_btn = new JButton("Add a row");
		addSoldSupplyRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		addSoldSupplyRow_btn.setBounds(27, 495, 222, 29);
		addSoldSupplyRow_btn.addActionListener(this);
		getContentPane().add(addSoldSupplyRow_btn);

		removeSoldSupplyRow_btn = new JButton("Delete selected row");
		removeSoldSupplyRow_btn.setFont(new Font("SetoFont", Font.PLAIN, 18));
		removeSoldSupplyRow_btn.setBounds(249, 495, 222, 29);
		removeSoldSupplyRow_btn.addActionListener(this);
		getContentPane().add(removeSoldSupplyRow_btn);

		portNumber_label = new JLabel("Port Number : 1111");
		portNumber_label.setFont(new Font("SetoFont", Font.PLAIN, 18));
		portNumber_label.setForeground(Color.RED);
		portNumber_label.setBounds(236, 17, 174, 20);
		getContentPane().add(portNumber_label);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addSupplyRow_btn) {
			addSupplyRow_btn_Click();
		} else if (e.getSource() == removeSupplyRow_btn) {
			removeSupplyRow_btn_Click();
		} else if (e.getSource() == saveProvider_btn) {
			saveProvider_btn_Click();
		} else if (e.getSource() == loadProvider_btn) {
			loadProvider_btn_Click();
		} else if (e.getSource() == addSoldSupplyRow_btn) {
			addSoldSupplyRow_btn_Click();
		} else if (e.getSource() == removeSoldSupplyRow_btn) {
			removeSoldSupplyRow_btn_Click();
		}
	}

	private void addSupplyRow_btn_Click() {
		supplyModel.addRow(new Object[] { "", "", "" });
	}

	private void removeSupplyRow_btn_Click() {
		int[] selectedRows = supply_table.getSelectedRows();
		for (int i = selectedRows.length - 1; i >= 0; i--) {
			supplyModel.removeRow(selectedRows[i]);
		}
	}

	private void addSoldSupplyRow_btn_Click() {
		soldModel.addRow(new Object[] { "", "" });
	}

	private void removeSoldSupplyRow_btn_Click() {
		int[] selectedRows = itemSold_table.getSelectedRows();
		for (int i = selectedRows.length - 1; i >= 0; i--) {
			soldModel.removeRow(selectedRows[i]);
		}
	}

	private void saveProvider_btn_Click() {
		try {
			// - - - - - - - - - - - - - - - - - - - - -
			// save distributor value
			String tName = getProviderName();
			providerDroid.instance.name = tName;
			providerDroid.instance.contact = contactInfo_textbox.getText();
			providerDroid.instance.address = address_textbox.getText();

			refreshSupplyList();

			infoAppend("將保存以下資訊*********\n" + providerDroid.instance.toString());

			// - - - - - - - - - - - - - - - - - - - - -
			// save process
			String toSaveFileName = JOptionPane.showInputDialog(mainPage, "幫檔案取個名字吧\n(預設為: \"" + tName + "\")");
			if (toSaveFileName == null) {
				return;
			} else if (toSaveFileName.isEmpty()) {
				toSaveFileName = tName;
			}

			if (new File(providerDroid.fileDir + toSaveFileName).exists()) {
				String[] options = { "蓋掉它", "還是算了" };
				int opt = JOptionPane.showOptionDialog(mainPage, "有一樣的檔案了，要不要覆蓋，要嗎要嗎?", "訊息", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, options, "蓋掉它");
				if (opt == JOptionPane.NO_OPTION) {
					return;
				}
			}
			infoAppend("檔案已保存，檔名為: \"" + toSaveFileName + ".xml\"");
			providerDroid.toXmlFile(toSaveFileName);

		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(mainPage, "你看看是不是表格有空白 (◔ д◔)\n不然就是該打數字 你打成文字");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(mainPage, e);
		}
	}

	private void loadProvider_btn_Click() {
		try {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(new File(providerDroid.fileDir));
			int returnValue = fileChooser.showOpenDialog(mainPage);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fileChooser.getSelectedFile();
				providerDroid.readXmlFile(selectedFile.getAbsolutePath());
				infoAppend("載入檔案，檔名為: \"" + selectedFile.getName() + "\"");
			}
			// - - - - - - - - - - - - - - - - - - - - -
			// set company's info
			companyName_textbox.setText(providerDroid.instance.name);
			contactInfo_textbox.setText(providerDroid.instance.contact);
			address_textbox.setText(providerDroid.instance.address);
			// - - - - - - - - - - - - - - - - - - - - -
			// clean and add supply to table
			Vector<Supply> supplyList = providerDroid.instance.getSupplyList();

			for (int row = supplyModel.getRowCount() - 1; row >= 0; row--) {
				supplyModel.removeRow(row);
			}

			supplyList.forEach(s -> {
				supplyModel.addRow(new Object[] { s.name, s.price, s.amountAvailable });
			});

		} catch (Exception e) {
			JOptionPane.showMessageDialog(mainPage, e);
		}
	}

	public String getProviderName() {
		return companyName_textbox.getText();
	}

	public void refreshSupplyList() {
		providerDroid.instance.clearSupplyList();

		for (Object o : supplyModel.getDataVector()) {
			try {
				Vector v = (Vector) o;
				Supply s = new Supply();
				s.name = v.get(0).toString();
				s.price = Double.parseDouble(v.get(1).toString());
				s.amountAvailable = Integer.parseInt(v.get(2).toString());
				providerDroid.instance.addSupply(s);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public void addToSoldList(String companyName, Vector<Supply> soldList) {
		System.out.println("sold!!!");
		for (Supply s : soldList) {
			soldModel.addRow(new Object[] { companyName, s.name, s.amountAvailable });
		}
	}

	public void setPortNum(int inpPort) {
		this.PortNo = inpPort;
		portNumber_label.setText("Port Number: " + this.PortNo);
	}

	public void infoAppend(String str) {
		info_TextArea.append(str + "\n");
	}

}